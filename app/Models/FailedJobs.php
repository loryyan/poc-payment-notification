<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailedJobs extends Model
{
    const METHOD_BCA = 'bca';

    protected $table = 'failed_jobs';
}
