<?php

namespace App\Listeners;

use App\Models\FailedJobs;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationListener implements ShouldQueue
{
    public function handle($event)
    {
        $data = json_decode($event->data);
        switch ($data->payment_method) {
            case 'bca':
                // handle paid payment  
                // return true
                break;
        }

        // if the data is unhandled, save into failed jobs table
        $this->saveFailedJob($data);
    }

    private function saveFailedJob($data): bool
    {
        $model = new FailedJobs;
        $model->payment_method = $data->payment_method ?? null;
        $model->header = $data->header ? json_encode($data->header) : null;
        $model->body = $data->body ? json_encode($data->header) : null;
        return $model->save();
    }
}