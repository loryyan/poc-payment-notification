<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    const PAYMENT_METHOD = 'bca';

    public function bca(Request $request)
    {
        $data = [
            'payment_method' => self::PAYMENT_METHOD,
            'header' => $request->header(),
            'body' => $request->all(),
        ];

        event(new \App\Events\BcaNotificationEvent(json_encode($data)));
        return "done";

    }
}
