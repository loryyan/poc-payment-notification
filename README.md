# poc payment notification

PoC of lumen with message broker to process the payment notifications

## Prerequisite

- mysql (latest)
- rabbitMQ (v.3.9.4)

## Installation

- copy the `.env.example` into `.env` and adjust the config based on your mysql and rabbitMQ settings
- adjust the `rabbitmq` section on the `config/queue.php` 
- run `php artisan migrate` and make sure `failed_jobs` table is created

## How To Test

- make sure rabbitMQ service is already started
- as a producer, hit `{url}/notification/bca using` using POST method and send any dummy notification body with the dummy headers. If the process is success, we will see the number of queues recorded in message broker. (`http://localhost:15672/#/queues`)
- we can run the consumer using this command: `php artisan rabbitmq:consume`. If the process is success, we will see the data in the `failed_jobs` table, because currently the data is ignored and directly saved in `failed_jobs`.
